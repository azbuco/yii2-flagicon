<?php

namespace azbuco\flagicon;

use yii\web\AssetBundle;

class FlagiconAsset extends AssetBundle
{
    public $sourcePath = '@bower/flag-icon-css';
    public $css = [
        'css/flag-icon.min.css',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
}
